import React from "react";
import styles from "./Button.module.scss";
export default function Button(props: any) {
  return (
    <div>
      <button
        className={`${styles.button} ${styles[props.color]}`}
        onClick={props.onClick}
        disabled={props.disabled}
      >
        {props.text}
      </button>
    </div>
  );
}
