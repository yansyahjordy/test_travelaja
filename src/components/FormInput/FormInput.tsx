import React from "react";
import styles from "./FormInput.module.scss";
export default function FormInput(props: any) {
  return (
    <div className={`${styles.inputWrapper} ${styles[props.width]}`}>
      <label>{props.label}</label>
      <input
        type={props.type}
        placeholder={props.placeholder}
        onChange={props.onChange}
        value={props.value}
        name={props.name}
        maxLength={props.maxLength}
      />
    </div>
  );
}
