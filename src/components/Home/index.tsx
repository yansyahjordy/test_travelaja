import styles from "./Home.module.scss";
import Image from "next/image";
import { useDispatch, useSelector } from "react-redux";
import {
  clearFormValues,
  clearUserData,
  setFormValues,
  setUserData,
} from "../../redux/input";
import { useEffect, useState } from "react";
import axios from "axios";
import FormInput from "../FormInput/FormInput";
import UserCard from "../UserCard/UserCard";
import Button from "../Button/Button";

export default function Home() {
  const userData = useSelector((state: any) => state.input.userData);
  const formValues = useSelector((state: any) => state.input.formValues);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [searchTerm, setSearchTerm] = useState<string>("");
  const [showPassword, setShowPassword] = useState<boolean>(false);
  const [errors, setErrors] = useState<any>({});
  const [generateError, setGenerateError] = useState<any>({});
  const [IsSubmit, setIsSubmit] = useState<boolean>(false);

  const dispatch = useDispatch();

  function clearForm() {
    dispatch(clearFormValues());
  }

  function clearUser() {
    dispatch(clearUserData());
  }

  async function fetchUser() {
    setIsLoading(true);
    setGenerateError("");
    try {
      await axios.get("https://randomuser.me/api").then((res: any) => {
        // console.log("res", res.data.results[0]);
        const data = res.data.results[0];
        dispatch(
          setUserData({
            name: `${data.name.title}. ${data.name.first} ${data.name.last}`,
            email: data.email,
            birthDate: data.dob.date,
            address: data.location.city,
            phone: data.phone,
            password: data.login.password,
            userImage: data.picture.medium,
          })
        );
      });
      setIsLoading(false);
    } catch (err: any) {
      setGenerateError({ generate: err.message });
      setIsLoading(false);
    }
  }

  function validate(formValues: any) {
    type Error = {
      name?: string;
      email?: string;
      birthDate?: string;
      address?: string;
      phone?: string;
      password?: string;
    };
    let error: Error = {};
    const regexPhone = /^62(\d{11})$/;
    const regexName = /^[a-zA-z]+ [a-zA-z]+$/;
    const regexEmail = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    const regexPassword =
      /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{6,}$/;

    if (!formValues.name) {
      error.name = "Name is Required";
    } else if (!regexName.test(formValues.name)) {
      error.name = "Name can only be alphabet with a space between";
    }
    if (!formValues.email) {
      error.email = "Email is Required";
    } else if (!regexEmail.test(formValues.email)) {
      error.email = "Must be a valid email address";
    }
    if (!formValues.birthDate) {
      error.birthDate = "Birth Date is Required";
    }
    if (!formValues.address) {
      error.address = "Address is Required";
    }
    if (!formValues.phone) {
      error.phone = "Phone is Required";
    } else if (formValues.phone.length !== 13) {
      error.phone = "Phone number length must be 13";
    } else if (!regexPhone.test(formValues.phone)) {
      error.phone = "Phone number length must be 13 with 62 in the beginning";
    }
    if (!formValues.password) {
      error.password = "Password is Required";
    } else if (formValues.password.length < 6) {
      error.password = "Minimum password length is 6";
    } else if (!regexPassword.test(formValues.password)) {
      error.password =
        "Password need minimum 6 chars contain at least 1 upper & lower case, 1 number, and 1 symbol";
    }
    return error;
  }

  function postUser() {
    setErrors(validate(formValues));
    setIsSubmit(true);
  }

  function handleChange(e: any) {
    const { value, name } = e.target;
    dispatch(setFormValues({ [name]: value }));
  }

  useEffect(() => {
    if (Object.keys(errors).length === 0 && IsSubmit) {
      dispatch(setUserData(formValues));
      clearForm();
      setIsSubmit(false);
    }
  }, [errors]);

  return (
    <>
      <div className={styles.containerTop}>
        <h1 className={styles.header}>Personal information</h1>
        <h2 className={styles.headerInfo}>
          This information will be displayed publicly so be careful what you
          share.
        </h2>

        <FormInput
          label="Full Name"
          type="text"
          placeholder="Full Name"
          onChange={(e) => handleChange(e)}
          value={formValues.name}
          width="medium"
          name="name"
          maxLength={50}
        />
        <p className={styles.errors}>{errors.name}</p>

        <FormInput
          label="Email Address"
          type="email"
          placeholder="yourmail@mail.com"
          onChange={(e) => handleChange(e)}
          value={formValues.email}
          width="medium"
          name="email"
        />
        <p className={styles.errors}>{errors.email}</p>

        <FormInput
          label="Date of Birth"
          type="date"
          placeholder=""
          onChange={(e) => handleChange(e)}
          value={formValues.birthDate}
          width="medium"
          name="birthDate"
        />
        <p className={styles.errors}>{errors.birthDate}</p>

        <FormInput
          label="Address"
          type="text"
          placeholder="Street Address"
          onChange={(e) => handleChange(e)}
          value={formValues.address}
          width="large"
          name="address"
          maxLength={255}
        />
        <p className={styles.errors}>{errors.address}</p>

        <div className={styles.inputPhoneWrapper}>
          <label>Phone Number</label>
          <div className={styles.inputPhone}>
            <div className={styles.phoneInfo}>+62</div>
            <input
              type="number"
              placeholder="e.g. 62 813 2611 2993"
              onChange={(e) => handleChange(e)}
              value={formValues.phone}
              name="phone"
              maxLength={13}
            />
          </div>
        </div>
        <p className={styles.errors}>{errors.phone}</p>

        <div className={styles.inputPasswordWrapper}>
          <label>Password</label>
          <div className={styles.passwordWrapper}>
            <div
              className={styles.icon}
              onClick={() => setShowPassword(!showPassword)}
            >
              <Image src="/img/eye-slash.svg" width={"20px"} height={"20px"} />
            </div>
            <input
              type={showPassword ? "text" : "password"}
              placeholder="**********"
              onChange={(e) => handleChange(e)}
              value={formValues.password}
              name="password"
            />
          </div>
          <p>
            Minimum of 6 characters, with upper & lower case, a number and a
            symbol.
          </p>
        </div>
        <p className={styles.errors}>{errors.password}</p>

        <div className={styles.line}></div>
        <div className={styles.buttonWrapper}>
          <div className={styles.left}>
            <Button
              text="Cancel"
              color="white"
              onClick={() => clearForm()}
              disabled={isLoading}
            />
            <Button
              text="Submit"
              color="indigo"
              onClick={() => postUser()}
              disabled={isLoading}
            />
          </div>
          <Button
            text={
              generateError.generate
                ? generateError.generate
                : isLoading
                ? "...Loading"
                : "Auto Generate"
            }
            color="blue"
            onClick={() => fetchUser()}
            disabled={isLoading}
          />
        </div>
      </div>
      <div className={styles.containerBottom}>
        <div className={styles.middleText}>
          <div className={styles.line}></div>
          <button
            onClick={() => clearUser()}
            style={
              userData.length === 0
                ? { color: "#979797" }
                : { color: "#E54B41" }
            }
          >
            Clear All List User
          </button>
        </div>

        <div className={styles.search}>
          <div className={styles.searchIcon}>
            <Image src="/img/search-normal.png" layout="fill"></Image>
          </div>
          <input
            type="text"
            placeholder="Search Anything"
            onChange={(e) => setSearchTerm(e.target.value)}
            value={searchTerm}
          />
        </div>

        {userData.length === 0 ? (
          <div className={styles.emptyWrapper}>
            <div className={styles.img}>
              <Image
                src="/img/noContent.png"
                width={"300px"}
                height={"300px"}
              ></Image>
            </div>
            <h1>No List User</h1>
          </div>
        ) : (
          <div className={styles.cardWrapper}>
            {userData
              .filter((user: any) => {
                if (searchTerm === "") {
                  return true;
                } else {
                  return user.name
                    .toLowerCase()
                    .includes(searchTerm.toLowerCase());
                }
              })
              .map((data: any, key: any) => (
                <UserCard data={data} key={key} />
              ))}
          </div>
        )}
      </div>
    </>
  );
}
