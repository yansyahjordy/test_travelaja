import React, { useState } from "react";
import styles from "./UserCard.module.scss";
import Image from "next/image";
import UserCardIcon from "../UserCardIcon/UserCardIcon";
export default function UserCard(props: any) {
  const [activeIcon, setActiveIcon] = useState("name");
  return (
    <div className={styles.cardWrapper}>
      <div className={styles.top}>
        {props.data.userImage === "" ? (
          <div className={styles.img}>
            <Image src="/img/placeholder.png" layout="fill"></Image>
          </div>
        ) : (
          <div className={styles.img}>
            <Image src={props.data.userImage} layout="fill"></Image>
          </div>
        )}
        {activeIcon === "name" ? (
          <>
            <h3>Hi, My name is</h3>
            <h2>{props.data.name}</h2>
          </>
        ) : (
          ""
        )}
        {activeIcon === "email" ? (
          <>
            <h3>My email address is</h3>
            <h2>{props.data.email}</h2>
          </>
        ) : (
          ""
        )}
        {activeIcon === "date" ? (
          <>
            <h3>My birthday is</h3>
            <h2>{props.data.birthDate}</h2>
          </>
        ) : (
          ""
        )}
        {activeIcon === "address" ? (
          <>
            <h3>My address is</h3>
            <h2>{props.data.address}</h2>
          </>
        ) : (
          ""
        )}
        {activeIcon === "phone" ? (
          <>
            <h3>My phone number is</h3>
            <h2>{props.data.phone}</h2>
          </>
        ) : (
          ""
        )}
        {activeIcon === "password" ? (
          <>
            <h3>My password is</h3>
            <h2>{props.data.password}</h2>
          </>
        ) : (
          ""
        )}
      </div>
      <div className={styles.bottom}>
        <div onClick={() => setActiveIcon("name")}>
          <UserCardIcon
            image={
              activeIcon === "name"
                ? "/img/user-tag-red.svg"
                : "/img/user-tag.svg"
            }
            active={activeIcon === "name" ? true : false}
          />
        </div>
        <div onClick={() => setActiveIcon("email")}>
          <UserCardIcon
            image={activeIcon === "email" ? "/img/sms-red.svg" : "/img/sms.svg"}
            active={activeIcon === "email" ? true : false}
          />
        </div>
        <div onClick={() => setActiveIcon("date")}>
          <UserCardIcon
            image={
              activeIcon === "date"
                ? "/img/calendar-red.svg"
                : "/img/calendar.svg"
            }
            active={activeIcon === "date" ? true : false}
          />
        </div>
        <div onClick={() => setActiveIcon("address")}>
          <UserCardIcon
            image={
              activeIcon === "address" ? "/img/map-red.svg" : "/img/map.svg"
            }
            active={activeIcon === "address" ? true : false}
          />
        </div>
        <div onClick={() => setActiveIcon("phone")}>
          <UserCardIcon
            image={
              activeIcon === "phone" ? "/img/mobile-red.svg" : "/img/mobile.svg"
            }
            active={activeIcon === "phone" ? true : false}
          />
        </div>
        <div onClick={() => setActiveIcon("password")}>
          <UserCardIcon
            image={
              activeIcon === "password" ? "/img/lock-red.svg" : "/img/lock.svg"
            }
            active={activeIcon === "password" ? true : false}
          />
        </div>
      </div>
    </div>
  );
}
