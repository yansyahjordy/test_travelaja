import React from "react";
import styles from "./UserCardIcon.module.scss";
import Image from "next/image";

export default function UserCardIcon(props: any) {
  return (
    <div className={styles.sectionWrapper}>
      {props.active ? (
        <div className={styles.redLine}>
          <Image src="/img/arrowUp.svg" layout="fill"></Image>
        </div>
      ) : (
        <div className={styles.line}> </div>
      )}
      <div className={styles.section}>
        <div className={styles.icon}>
          <Image src={props.image} layout="fill"></Image>
        </div>
      </div>
    </div>
  );
}
