import React from "react";
import Home from "../components/Home";
export default function index() {
  return (
    <>
      <div className="app">
        <Home />
      </div>
    </>
  );
}
