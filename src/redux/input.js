import { createSlice } from "@reduxjs/toolkit";

export const inputSlice = createSlice({
  name: "input",
  initialState: {
    userData: [],
    formValues: {
      name: "",
      email: "",
      birthDate: "",
      address: "",
      phone: "",
      password: "",
      userImage: "",
    },
  },
  reducers: {
    setUserData: (state, action) => {
      if (state.userData.length === 0) {
        state.userData = [action.payload];
      } else {
        state.userData = [...state.userData, action.payload];
      }
    },
    clearUserData: (state) => {
      state.userData = [];
    },

    setFormValues: (state, action) => {
      let newData = action.payload;
      state.formValues = { ...state.formValues, ...newData };
    },
    clearFormValues: (state) => {
      state.formValues = {
        name: "",
        email: "",
        birthDate: "",
        address: "",
        phone: "",
        password: "",
        userImage: "",
      };
    },
  },
});

export const { setUserData, clearUserData, setFormValues, clearFormValues } =
  inputSlice.actions;

export default inputSlice.reducer;
